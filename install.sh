#!/bin/bash

cp mslab-status.service /etc/systemd/system/
cp mslab-status.timer /etc/systemd/system/

vim /etc/systemd/system/mslab-status.service

systemctl daemon-reload

systemctl enable mslab-status.timer
systemctl start mslab-status.timer

systemctl status mslab-status.timer
