#!/bin/bash

load=(`cat /proc/loadavg | cut -f '1 2 3' -d " "`)
memory_using=$(free | awk '/cache:/{print $3}')

curl -s -X PATCH -d "load1=${load[0]}&load5=${load[1]}&load15=${load[2]}&memory_using=${memory_using}&token=${token}" "https://mslab-status.herokuapp.com/api/machines"
